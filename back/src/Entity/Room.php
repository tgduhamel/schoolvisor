<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 30/12/18
 * Time: 22:22
 */

namespace App\Entity;


class Room
{
    private $id;

    /**
     * @var integer
     */
    private $salle;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSalle(): int
    {
        return $this->salle;
    }

    /**
     * @param int $salle
     */
    public function setSalle(int $salle): void
    {
        $this->salle = $salle;
    }
}