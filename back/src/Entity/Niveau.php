<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 30/12/18
 * Time: 21:52
 */

namespace App\Entity;


class Niveau
{
    private $id;

    private $niveau;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * @param mixed $niveau
     */
    public function setNiveau($niveau): void
    {
        $this->niveau = $niveau;
    }

}