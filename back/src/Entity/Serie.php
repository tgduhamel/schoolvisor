<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 30/12/18
 * Time: 21:26
 */

namespace App\Entity;


class Serie
{
    private $id;

    private $serie;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param mixed $serie
     */
    public function setSerie($serie): void
    {
        $this->serie = $serie;
    }
}