<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 02/01/19
 * Time: 23:42
 */

namespace App\Entity;

class Discipline
{
    private $id;

    private $libelle;

    private $description;

    private $sanction;

    private $createOn;

    private $eleve;

    private $session;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getSanction()
    {
        return $this->sanction;
    }

    /**
     * @param mixed $sanction
     */
    public function setSanction($sanction): void
    {
        $this->sanction = $sanction;
    }

    /**
     * @return mixed
     */
    public function getCreateOn()
    {
        return $this->createOn;
    }

    /**
     * @param mixed $createOn
     */
    public function setCreateOn($createOn): void
    {
        $this->createOn = $createOn;
    }

    /**
     * @return mixed
     */
    public function getEleve()
    {
        return $this->eleve;
    }

    /**
     * @param mixed $eleve
     */
    public function setEleve($eleve): void
    {
        $this->eleve = $eleve;
    }

    /**
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param mixed $session
     */
    public function setSession($session): void
    {
        $this->session = $session;
    }

}