<?php

namespace App\Controller\Settings\Rooms;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 26/12/18
 * Time: 23:24
 */

class MainController extends Controller
{
    /**
     * @Route("/settings/rooms", name="rooms")
     */
    public function index()
    {
        return $this->render('settings/rooms/main.html.twig');
    }
}