<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Settings\Rooms;


use App\Service\NiveauService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/settings/rooms/list", name="rooms_list")
     */
    public function index(NiveauService $niveauService)
    {
        return $this->render('settings/rooms/list.html.twig', [
            'list' => $niveauService->list(),
        ]);
    }
}