<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Settings\Rooms;

use App\Form\RoomType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/settings/rooms/form/{id}", name="rooms_form")
     */
    public function index($id = null)
    {
        $form = $this->createForm(RoomType::class)->createView();
        return $this->render('settings/rooms/form.html.twig', [
            'oForm' => $form
        ]);
    }
}