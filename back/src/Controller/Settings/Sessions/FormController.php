<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Settings\Sessions;


use App\Form\SessionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/settings/sessions/form/{id}", name="sessions_form")
     */
    public function index($id = null)
    {
        $form = $this->createForm(SessionType::class)->createView();
        return $this->render('settings/sessions/form.html.twig', [
            'oForm' => $form
        ]);
    }
}