<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Settings\Sessions;


use App\Service\SchoolService;
use App\Service\SessionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/settings/sessions/list", name="sessions_list")
     */
    public function index(SessionService $sessionService)
    {
        return $this->render('settings/sessions/list.html.twig', [
            'list' => $sessionService->list(),
        ]);
    }
}