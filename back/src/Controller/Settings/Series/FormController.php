<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Settings\Series;


use App\Form\SerieType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/settings/series/form/{id}", name="series_form")
     */
    public function index($id = null)
    {
        $form = $this->createForm(SerieType::class)->createView();
        return $this->render('settings/series/form.html.twig', [
            'oForm' => $form
        ]);
    }
}