<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Settings\Level;


use App\Service\NiveauService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/settings/levels/list", name="levels_list")
     */
    public function index(NiveauService $niveauService)
    {
        return $this->render('settings/levels/list.html.twig', [
            'list' => $niveauService->list(),
        ]);
    }
}