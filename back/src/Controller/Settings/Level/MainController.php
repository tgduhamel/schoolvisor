<?php

namespace App\Controller\Settings\Level;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 26/12/18
 * Time: 23:24
 */

class MainController extends Controller
{
    /**
     * @Route("/settings/levels", name="levels")
     */
    public function index()
    {
        return $this->render('settings/levels/main.html.twig');
    }
}