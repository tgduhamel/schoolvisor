<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Settings\Level;


use App\Form\NiveauType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/settings/levels/form/{id}", name="levels_form")
     */
    public function index($id = null)
    {
        $form = $this->createForm(NiveauType::class)->createView();
        return $this->render('settings/levels/form.html.twig', [
            'oForm' => $form
        ]);
    }
}