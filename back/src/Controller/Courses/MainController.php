<?php

namespace App\Controller\Courses;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 26/12/18
 * Time: 23:24
 */

class MainController extends Controller
{
    /**
     * @Route("/courses", name="courses")
     */
    public function index()
    {
        return $this->render('courses/main.html.twig');
    }
}