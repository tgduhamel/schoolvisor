<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Courses;


use App\Form\CourseType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/courses/form/{id}", name="courses_form")
     */
    public function index($id = null)
    {
        $form = $this->createForm(CourseType::class)->createView();
        return $this->render('courses/form.html.twig', [
            'oForm' => $form
        ]);
    }
}