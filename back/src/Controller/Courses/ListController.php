<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Courses;


use App\Service\CourseService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/courses/list", name="courses_list")
     */
    public function index(CourseService $courseService)
    {
        return $this->render('courses/list.html.twig', [
            'list' => $courseService->list(),
        ]);
    }
}