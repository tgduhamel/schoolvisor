<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Classes\Classes;

use App\Service\AbscenceService;
use App\Service\ClasseService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/classes/list", name="classes_list")
     */
    public function index(ClasseService $classeService)
    {
        return $this->render('classes/classes/list.html.twig', [
            'list' => $classeService->list(),
        ]);
    }
}