<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Classes\Classes;


use App\Form\ClasseType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/classes/form/{id}", name="classes_form")
     */
    public function index($id = null)
    {
        $form = $this->createForm(ClasseType::class)->createView();
        return $this->render('classes/classes/form.html.twig', [
            'oForm' => $form
        ]);
    }
}