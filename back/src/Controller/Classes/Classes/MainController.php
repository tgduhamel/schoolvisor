<?php

namespace App\Controller\Classes\Classes;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 26/12/18
 * Time: 23:48
 */

class MainController extends Controller
{
    /**
     * @Route("/classes", name="classes")
     */
    public function index()
    {
        return $this->render('classes/classes/main.html.twig');
    }
}