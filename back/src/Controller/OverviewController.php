<?php

namespace App\Controller;

use App\Service\AbscenceService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Unirest\Method;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 12/12/18
 * Time: 23:43
 */
class OverviewController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(AbscenceService $abscenceService)
    {
        $response = $abscenceService->requestApi(Method::GET, getenv('API_URL') . "series");

        return $this->render('base.html.twig');
    }
}