<?php

namespace App\Controller\Schools;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 26/12/18
 * Time: 23:24
 */

class MainController extends Controller
{
    /**
     * @Route("/schools", name="schools")
     */
    public function index()
    {
        return $this->render('schools/main.html.twig');
    }
}