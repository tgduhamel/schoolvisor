<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Schools;


use App\Service\SchoolService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/schools/list", name="schools_list")
     */
    public function index(SchoolService $schoolService)
    {
        return $this->render('schools/list.html.twig', [
            'list' => $schoolService->list(),
        ]);
    }
}