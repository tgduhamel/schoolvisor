<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Students\Students;


use App\Form\SchoolType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/students/form/{id}", name="students_form")
     */
    public function index(SchoolType $schoolType, $id = null)
    {
        $form = $this->createForm(SchoolType::class)->createView();
        return $this->render('students/students/form.html.twig', [
            'oForm' => $form
        ]);
    }
}