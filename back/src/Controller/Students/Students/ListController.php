<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Students\Students;


use App\Service\SchoolService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/students/list", name="students_list")
     */
    public function index(SchoolService $schoolService)
    {
        return $this->render('students/students/list.html.twig', [
            'list' => $schoolService->list(),
        ]);
    }
}