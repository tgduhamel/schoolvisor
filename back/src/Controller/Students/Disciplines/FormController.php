<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Students\Disciplines;


use App\Form\DisciplineType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/disciplines/form/{id}", name="disciplines_form")
     */
    public function index($id = null)
    {
        $form = $this->createForm(DisciplineType::class)->createView();
        return $this->render('students/disciplines/form.html.twig', [
            'oForm' => $form
        ]);
    }
}