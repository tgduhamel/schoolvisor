<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Students\Disciplines;

use App\Service\DisciplineService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/disciplines/list", name="disciplines_list")
     */
    public function index(DisciplineService $disciplineService)
    {
        return $this->render('students/disciplines/list.html.twig', [
            'list' => $disciplineService->list(),
        ]);
    }
}