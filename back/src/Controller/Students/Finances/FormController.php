<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Students\Finances;


use App\Form\FinanceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/finances/form/{id}", name="finances_form")
     */
    public function index($id = null)
    {
        $form = $this->createForm(FinanceType::class)->createView();
        return $this->render('students/finances/form.html.twig', [
            'oForm' => $form
        ]);
    }
}