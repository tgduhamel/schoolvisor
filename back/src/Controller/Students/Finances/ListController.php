<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Students\Finances;

use App\Service\DisciplineService;
use App\Service\FinanceService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/finances/list", name="finances_list")
     */
    public function index(FinanceService $financeService)
    {
        return $this->render('students/finances/list.html.twig', [
            'list' => $financeService->list(),
        ]);
    }
}