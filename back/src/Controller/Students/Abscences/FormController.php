<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 27/12/18
 * Time: 23:22
 */

namespace App\Controller\Students\Abscences;


use App\Form\AbscenceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class FormController extends Controller
{
    /**
     * @Route("/abscences/form/{id}", name="abscences_form")
     */
    public function index($id = null)
    {
        $form = $this->createForm(AbscenceType::class)->createView();
        return $this->render('students/abscences/form.html.twig', [
            'oForm' => $form
        ]);
    }
}