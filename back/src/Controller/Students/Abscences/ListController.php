<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 13:24
 */

namespace App\Controller\Students\Abscences;

use App\Service\AbscenceService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends Controller
{
    /**
     * @Route("/abscences/list", name="abscences_list")
     */
    public function index(AbscenceService $abscenceService)
    {
        return $this->render('students/abscences/list.html.twig', [
            'list' => $abscenceService->list(),
        ]);
    }
}