<?php

namespace App\Controller\Students\Abscences;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 26/12/18
 * Time: 23:48
 */

class MainController extends Controller
{
    /**
     * @Route("/abscences", name="abscences")
     */
    public function index()
    {
        return $this->render('students/abscences/main.html.twig');
    }
}