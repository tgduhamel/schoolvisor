<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 04/01/19
 * Time: 00:08
 */

namespace App\Service;

use Unirest\Method;
use Unirest\Request;


abstract class BaseService
{
    /**
     * @param $method
     * @param $url
     * @param $body
     * @return \Unirest\Response
     * @throws \Unirest\Exception
     */
    function requestApi($method, $url, $body = null)
    {
        $response = Request::send($method, $url, $body, array());
        return $response;
    }

}