<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 29/12/18
 * Time: 14:08
 */

namespace App\Form;

use App\Entity\School;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SchoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('quartier', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('ville', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('telephone', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('mail', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('url', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('postalCode', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => School::class,
        ]);
    }


}