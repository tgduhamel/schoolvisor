<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 03/01/19
 * Time: 08:19
 */

namespace App\Form;


use App\Entity\Finance;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FinanceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('montant', MoneyType::class, [
                'required' => true,
                'grouping' => true,
                'currency' => 'XAF',
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('detail', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('datePaiement', DateType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('eleve', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('session', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Finance::class,
        ]);
    }
}