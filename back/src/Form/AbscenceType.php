<?php
/**
 * Created by PhpStorm.
 * User: iamtchafack
 * Date: 03/01/19
 * Time: 19:18
 */

namespace App\Form;


use App\Entity\Abscence;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AbscenceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('debut', DateTimeType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('fin', DateTimeType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('eleve', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('matiere', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('session', TextType::class, [
                'required' => true,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Abscence::class,
        ]);
    }
}