<?php

namespace App\Repository;

use App\Entity\Devoirs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Devoirs|null find($id, $lockMode = null, $lockVersion = null)
 * @method Devoirs|null findOneBy(array $criteria, array $orderBy = null)
 * @method Devoirs[]    findAll()
 * @method Devoirs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DevoirsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Devoirs::class);
    }

    // /**
    //  * @return Devoirs[] Returns an array of Devoirs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Devoirs
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
