<?php

namespace App\Repository;

use App\Entity\Colleges;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Colleges|null find($id, $lockMode = null, $lockVersion = null)
 * @method Colleges|null findOneBy(array $criteria, array $orderBy = null)
 * @method Colleges[]    findAll()
 * @method Colleges[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CollegesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Colleges::class);
    }

    // /**
    //  * @return Colleges[] Returns an array of Colleges objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Colleges
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
