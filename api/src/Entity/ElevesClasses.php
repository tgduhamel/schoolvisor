<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ElevesClassesRepository")
 */
class ElevesClasses
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Sessions
     * @ORM\ManyToOne(targetEntity="Sessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

    /**
     * @var Colleges
     * @ORM\ManyToOne(targetEntity="Colleges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $college;

    /**
     * @var Classes
     * @ORM\ManyToOne(targetEntity="classes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classe;

    /**
     * @var Eleves
     * @ORM\ManyToOne(targetEntity="Eleves")
     * @ORM\JoinColumn(nullable=false)
     */
    private $eleve;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Sessions
     */
    public function getSession(): Sessions
    {
        return $this->session;
    }

    /**
     * @param Sessions $session
     */
    public function setSession(Sessions $session): void
    {
        $this->session = $session;
    }

    /**
     * @return Colleges
     */
    public function getCollege(): Colleges
    {
        return $this->college;
    }

    /**
     * @param Colleges $college
     */
    public function setCollege(Colleges $college): void
    {
        $this->college = $college;
    }

    /**
     * @return Classes
     */
    public function getClasse(): Classes
    {
        return $this->classe;
    }

    /**
     * @param Classes $classe
     */
    public function setClasse(Classes $classe): void
    {
        $this->classe = $classe;
    }

    /**
     * @return Eleves
     */
    public function getEleve(): Eleves
    {
        return $this->eleve;
    }

    /**
     * @param Eleves $eleve
     */
    public function setEleve(Eleves $eleve): void
    {
        $this->eleve = $eleve;
    }
}
