<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\NotesRepository")
 */
class Notes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $note;

    /**
     * @var Devoirs
     * @ORM\ManyToOne(targetEntity="Devoirs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $devoir;

    /**
     * @var Matieres
     * @ORM\ManyToOne(targetEntity="Matieres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $matiere;

    /**
     * @var Eleves
     * @ORM\ManyToOne(targetEntity="Eleves")
     * @ORM\JoinColumn(nullable=false)
     */
    private $eleve;

    /**
     * @var Sessions
     * @ORM\ManyToOne(targetEntity="Sessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNote(): ?float
    {
        return $this->note;
    }

    public function setNote(float $note): self
    {
        $this->note = $note;

        return $this;
    }

    /**
     * @return Devoirs
     */
    public function getDevoir(): Devoirs
    {
        return $this->devoir;
    }

    /**
     * @param Devoirs $devoir
     */
    public function setDevoir(Devoirs $devoir): void
    {
        $this->devoir = $devoir;
    }

    /**
     * @return Matieres
     */
    public function getMatiere(): Matieres
    {
        return $this->matiere;
    }

    /**
     * @param Matieres $matiere
     */
    public function setMatiere(Matieres $matiere): void
    {
        $this->matiere = $matiere;
    }

    /**
     * @return Eleves
     */
    public function getEleve(): Eleves
    {
        return $this->eleve;
    }

    /**
     * @param Eleves $eleve
     */
    public function setEleve(Eleves $eleve): void
    {
        $this->eleve = $eleve;
    }

    /**
     * @return Sessions
     */
    public function getSession(): Sessions
    {
        return $this->session;
    }

    /**
     * @param Sessions $session
     */
    public function setSession(Sessions $session): void
    {
        $this->session = $session;
    }

}
