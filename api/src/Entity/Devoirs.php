<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\DevoirsRepository")
 */
class Devoirs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateDevoir;

    /**
     * @ORM\Column(type="integer")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $detail;

    /**
     * @var Colleges
     * @ORM\ManyToOne(targetEntity="Colleges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $college;

    /**
     * @var Classes
     * @ORM\ManyToOne(targetEntity="classes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classes;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateRemiseNotes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateDevoir(): ?\DateTimeInterface
    {
        return $this->dateDevoir;
    }

    public function setDateDevoir(\DateTimeInterface $dateDevoir): self
    {
        $this->dateDevoir = $dateDevoir;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(string $detail): self
    {
        $this->detail = $detail;

        return $this;
    }

    public function getDateRemiseNotes(): ?\DateTimeInterface
    {
        return $this->dateRemiseNotes;
    }

    public function setDateRemiseNotes(?\DateTimeInterface $dateRemiseNotes): self
    {
        $this->dateRemiseNotes = $dateRemiseNotes;

        return $this;
    }

    /**
     * @return Colleges
     */
    public function getCollege(): Colleges
    {
        return $this->college;
    }

    /**
     * @param Colleges $college
     */
    public function setCollege(Colleges $college): void
    {
        $this->college = $college;
    }

    /**
     * @return Classes
     */
    public function getClasses(): Classes
    {
        return $this->classes;
    }

    /**
     * @param Classes $classes
     */
    public function setClasses(Classes $classes): void
    {
        $this->classes = $classes;
    }

}
