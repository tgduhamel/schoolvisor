<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\PlanningsRepository")
 */
class Plannings
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titulaire;

    /**
     * @ORM\Column(type="integer")
     */
    private $effectif;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $scolarite;

    /**
     * @var Sessions
     * @ORM\ManyToOne(targetEntity="Sessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

    /**
     * @var Colleges
     * @ORM\ManyToOne(targetEntity="Colleges")
     * @ORM\JoinColumn(nullable=false)
     */
    private $college;

    /**
     * @var Classes
     * @ORM\ManyToOne(targetEntity="classes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classe;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getTitulaire(): ?string
    {
        return $this->titulaire;
    }

    public function setTitulaire(?string $titulaire): self
    {
        $this->titulaire = $titulaire;

        return $this;
    }

    public function getEffectif(): ?int
    {
        return $this->effectif;
    }

    public function setEffectif(int $effectif): self
    {
        $this->effectif = $effectif;

        return $this;
    }

    public function getScolarite()
    {
        return $this->scolarite;
    }

    public function setScolarite($scolarite): self
    {
        $this->scolarite = $scolarite;

        return $this;
    }

    /**
     * @return Sessions
     */
    public function getSession(): Sessions
    {
        return $this->session;
    }

    /**
     * @param Sessions $session
     */
    public function setSession(Sessions $session): void
    {
        $this->session = $session;
    }

    /**
     * @return Colleges
     */
    public function getCollege(): Colleges
    {
        return $this->college;
    }

    /**
     * @param Colleges $college
     */
    public function setCollege(Colleges $college): void
    {
        $this->college = $college;
    }

    /**
     * @return Classes
     */
    public function getClasse(): Classes
    {
        return $this->classe;
    }

    /**
     * @param Classes $classe
     */
    public function setClasse(Classes $classe): void
    {
        $this->classe = $classe;
    }

}
