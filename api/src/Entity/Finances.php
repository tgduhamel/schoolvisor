<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\FinancesRepository")
 */
class Finances
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=2)
     */
    private $montant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $detail;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datePaiement;

    /**
     * @var Eleves
     * @ORM\ManyToOne(targetEntity="Eleves")
     * @ORM\JoinColumn(nullable=false)
     */
    private $eleve;

    /**
     * @var Sessions
     * @ORM\ManyToOne(targetEntity="Sessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMontant()
    {
        return $this->montant;
    }

    public function setMontant($montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getDetail(): ?string
    {
        return $this->detail;
    }

    public function setDetail(string $detail): self
    {
        $this->detail = $detail;

        return $this;
    }

    public function getDatePaiement(): ?\DateTimeInterface
    {
        return $this->datePaiement;
    }

    public function setDatePaiement(\DateTimeInterface $datePaiement): self
    {
        $this->datePaiement = $datePaiement;

        return $this;
    }

    /**
     * @return Eleves
     */
    public function getEleve(): Eleves
    {
        return $this->eleve;
    }

    /**
     * @param Eleves $eleve
     */
    public function setEleve(Eleves $eleve): void
    {
        $this->eleve = $eleve;
    }

    /**
     * @return Sessions
     */
    public function getSession(): Sessions
    {
        return $this->session;
    }

    /**
     * @param Sessions $session
     */
    public function setSession(Sessions $session): void
    {
        $this->session = $session;
    }

}
