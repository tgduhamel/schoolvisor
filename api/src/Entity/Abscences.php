<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\AbscencesRepository")
 */
class Abscences
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $debut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fin;

    /**
     * @var Eleves
     * @ORM\ManyToOne(targetEntity="Eleves")
     * @ORM\JoinColumn(nullable=false)
     */
    private $eleve;

    /**
     * @var Sessions
     * @ORM\ManyToOne(targetEntity="Sessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $session;

    /**
     * @var Matieres
     * @ORM\ManyToOne(targetEntity="Matieres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $matiere;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDebut(): ?\DateTimeInterface
    {
        return $this->debut;
    }

    public function setDebut(\DateTimeInterface $debut): self
    {
        $this->debut = $debut;

        return $this;
    }

    public function getFin(): ?\DateTimeInterface
    {
        return $this->fin;
    }

    public function setFin(\DateTimeInterface $fin): self
    {
        $this->fin = $fin;

        return $this;
    }

    /**
     * @return Eleves
     */
    public function getEleve(): Eleves
    {
        return $this->eleve;
    }

    /**
     * @param Eleves $eleve
     */
    public function setEleve(Eleves $eleve): void
    {
        $this->eleve = $eleve;
    }

    /**
     * @return Sessions
     */
    public function getSession(): Sessions
    {
        return $this->session;
    }

    /**
     * @param Sessions $session
     */
    public function setSession(Sessions $session): void
    {
        $this->session = $session;
    }

    /**
     * @return Matieres
     */
    public function getMatiere(): Matieres
    {
        return $this->matiere;
    }

    /**
     * @param Matieres $matiere
     */
    public function setMatiere(Matieres $matiere): void
    {
        $this->matiere = $matiere;
    }

}
